package edu.dsa.preyea;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Binary Search Implementation
 * @author Preyea R. Regmi
 *
 */
public class BinarySearch {



    public static void main(String[] args1) {
        /**
         * Create time stamp when this method is invoked from console.
         */
        final long startAllTime = System.nanoTime();

        List<Integer> inputSortedList = null;
        Integer searchParam = null;

        /**
         * Read Inputs from the user and prepare data.
         */

        System.out.println("Enter filename.");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            inputSortedList = loadFileToListOfInts(reader.readLine());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new IllegalArgumentException(e.getMessage()+ "\nInvalid file. Please verify the correctness of provided dataset.");
        }

        System.out.println("Enter search number.");

        try {
            searchParam= Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException e) {
            throw new IllegalArgumentException("Search parameter must be an integer");
        }


        /**
         * Create starting time stamp for evaluating total execution time of algorithm excluding IO operations.
         */
        final long startAlgoTime = System.nanoTime();

        int resultIndex = binarySearchIterative(inputSortedList, searchParam);
        /**
         * Evaluate the ending time of algorithm with respect to starting time.
         */
        final long endAlgoTime=(System.nanoTime() - startAlgoTime)/ 1000000;

        if (resultIndex != -1)
            System.out.println("Number " + searchParam + " found at index " + resultIndex);
        else
            System.out.println("Number not found("+resultIndex+")");

        /**
         * Print the execution time respectively.
         */
        System.out.println("Algo time : "+endAlgoTime+" milliseconds");
        System.out.println("All time : "+(System.nanoTime() - startAllTime)/ 1000000+" milliseconds");

    }

    private static List<Integer> loadFileToListOfInts(String filePath) throws Exception {
        List<Integer> sortedInputList = new ArrayList<>();
        /**
         * Create buffered reader to read the file line by line.
         */
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = br.readLine()) != null) {
            /**
             * For each read line,we try to parse the character as integer which are seperated by white space.
             * Its important  for the dataset to be in correct format, otherwise we are marking this function to throw Exception to the caller method.
             */
            for (String e : line.split(" ")) {
                sortedInputList.add(Integer.parseInt(e));
            }
        }
        br.close();
        return sortedInputList;
    }

    /**
     * We are attempting to perform binary search using iterative approach.
     */
    public static int binarySearchIterative(List<Integer> sortedArray, int searchInput) {
        int lowPointer = 0;
        int highPointer = sortedArray.size() - 1;
        int algoIteration=0;
        while (lowPointer <= highPointer) {
            if(algoIteration<4)
            {
                /**
                 * Print the low pointer and high pointer for the first 4 iterations.
                 */
                System.out.println("Iteration("+algoIteration+")");
                System.out.println("Low pointer :"+lowPointer);
                System.out.println("High pointer :"+highPointer);
                System.out.println(" ");
            }
            int midPointer = lowPointer + (highPointer - lowPointer) / 2;
            /**
             * The guessed number is the correct number. So, we return the midPointer as result.
             */
            if (sortedArray.get(midPointer) == searchInput)
                return midPointer;
            else if (sortedArray.get(midPointer) > searchInput) {
                /**
                 * The guessed number is too high, So we are concluding that the number lies in the lower partition. Hence, we discard high partition by
                 * updating the high pointer as below.
                 */
                highPointer = midPointer - 1;
            } else {
                /**
                 * The guessed number is too low, So we are concluding that the number lies in the higher partition. Hence, we discard lower partition by
                 * updating the low pointer as below.
                 */
                lowPointer = midPointer + 1;
            }
            algoIteration++;
        }
        /**
         * If above iteration fails to return result, we conclude the searched number doesn't exists in our dataset.
         */
        return -1;
    }
}
