
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math

# Plot canvas configuration initialize
plt.rcParams['figure.figsize'] = (12.0, 9.0)

def performRegressionAnalysisForAlgoTime():

  print("Algo time regression analysis")
  # Algo time data set from previous question solution.
  data_algo_time_respect_to_input_size = [[28, math.log10(100)], [27, math.log10(100)], [28, math.log10(100)],[19,math.log10(1000)],[28,math.log10(1000)],[21,math.log10(1000)],[29,math.log10(10000)],[28,math.log10(10000)],[28,math.log10(10000)],[26,math.log10(100000)],[27,math.log10(100000)],[29,math.log10(100000)]]

  df = pd.DataFrame(data_algo_time_respect_to_input_size, columns = ['Algo Execution Time', 'Input Size'])
  # Load input size to X-Axis
  # Load algo execution time to Y-Axis
  X = df.iloc[:, 1]
  Y = df.iloc[:, 0]
  
  plt.xlabel("Input Size")
  plt.ylabel("Algo Time")


  # Regression related calculation referred from 
  # https://towardsdatascience.com/linear-regression-using-least-squares-a4c3456e8570
  X_mean = np.mean(X)
  Y_mean = np.mean(Y)

  num = 0
  den = 0
  for i in range(len(X)):
    num += (X[i] - X_mean)*(Y[i] - Y_mean)
    den += (X[i] - X_mean)**2
  m = num / den
  c = Y_mean - m*X_mean

  Y_pred = m*X + c

  plt.scatter(X, Y) 


  plt.plot([min(X), max(X)], [min(Y_pred), max(Y_pred)], color='red') # predicted
  plt.show()
  
def performRegressionAnalysisForAllTime():
  

  print("All time regression analysis")
  # All time data set from previous question solution.
  data_all_time_respect_to_input_size = [[22654, math.log10(100)], [5701, math.log10(100)], [6518, math.log10(100)],[17046,math.log10(1000)],[7114,math.log10(1000)],[9042,math.log10(1000)],[20997,math.log10(10000)],[10812,math.log10(10000)],[8490,math.log10(10000)],[18620,math.log10(100000)],[9608,math.log10(100000)],[13797,math.log10(100000)]]

  df = pd.DataFrame(data_all_time_respect_to_input_size, columns = ['All Execution Time', 'Input Size'])
  # Load input size to X-Axis
  # Load all execution time to Y-Axis
  X = df.iloc[:, 1]
  Y = df.iloc[:, 0]

  plt.xlabel("Input Size")
  plt.ylabel("All Time")

  # Regression related calculation referred from 
  # https://towardsdatascience.com/linear-regression-using-least-squares-a4c3456e8570
  X_mean = np.mean(X)
  Y_mean = np.mean(Y)

  num = 0
  den = 0
  for i in range(len(X)):
    num += (X[i] - X_mean)*(Y[i] - Y_mean)
    den += (X[i] - X_mean)**2
  m = num / den
  c = Y_mean - m*X_mean

  Y_pred = m*X + c

  plt.scatter(X, Y) 


  plt.plot([min(X), max(X)], [min(Y_pred), max(Y_pred)], color='red') # predicted
  plt.show()  

# Perform regression analysis for algo execution time
performRegressionAnalysisForAlgoTime()

# Perform regression analysis for all execution time
performRegressionAnalysisForAllTime()





